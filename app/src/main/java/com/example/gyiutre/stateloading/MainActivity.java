package com.example.gyiutre.stateloading;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.loadingstate.StateLoad;

public class MainActivity extends AppCompatActivity {
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;


        final StateLoad stateLoad = (StateLoad) findViewById(R.id.stateLoad);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mContext, "Should be hidden", Toast.LENGTH_LONG).show();
                stateLoad.toContent();
            }
        },2000);

        Button button = (Button) findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateLoad.toLoad();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, "Should be hidden", Toast.LENGTH_LONG).show();
                        stateLoad.toContent();
                    }
                },2000);
            }
        });
    }
}
